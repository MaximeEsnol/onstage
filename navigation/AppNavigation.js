import React from 'react';
import {NavigationContainer} from "@react-navigation/native";
import {createStackNavigator} from "@react-navigation/stack";
import {createBottomTabNavigator} from "@react-navigation/bottom-tabs";

import {Ionicons} from "@expo/vector-icons";

import {Color, ComponentColor, DefaultTheme} from "../constants/Colors";
import Home from "../screens/Home";
import Search from "../screens/Search";
import Planned from "../screens/Planned";
import ArtistDetails from "../screens/ArtistDetails";
import Show from '../screens/Show';
import {findArtistById, findTourById} from '../func/Queries';
import Tour from '../screens/Tour';
import FollowedArtists from '../screens/FollowedArtists';

const Tab = createBottomTabNavigator();

const BottomNav = props => {
    return (
        <NavigationContainer theme={DefaultTheme}>
            <Tab.Navigator
                tabBarOptions={{
                    activeTintColor: ComponentColor.FOOTER_CONTENT_COLOR,
                    inactiveTintColor: ComponentColor.FOOTER_INACTIVE_COLOR,
                    style: {
                        backgroundColor: ComponentColor.FOOTER_COLOR
                    }
                }}

                screenOptions={({route}) => ({
                    tabBarIcon: ({focused, size}) => {
                        let icon;

                        if (route.name === "Home") {
                            icon = "ios-home";
                        } else if (route.name === "Search") {
                            icon = "ios-search";
                        } else if (route.name === "Planned") {
                            icon = "ios-calendar";
                        }

                        return <Ionicons name={icon} size={size} style={{
                            color: (focused) ? ComponentColor.FOOTER_CONTENT_COLOR : ComponentColor.FOOTER_INACTIVE_COLOR
                        }}/>;
                    }
                })}
            >
                <Tab.Screen name={"Home"} component={HomeStackNavigator}/>
                <Tab.Screen name={"Search"} component={SearchStackNavigator}/>
                <Tab.Screen name={"Planned"} component={PlannedStackNavigator}/>
            </Tab.Navigator>
        </NavigationContainer>
    )
};

const FollowedStack = createStackNavigator();

const FollowedStackNavigator = props => {
    props.navigation.setOptions({
        headerShown: false
    });

    return(
        <FollowedStack.Navigator>
            <FollowedStack.Screen name="Followed Artists" component={FollowedArtists}/>
            <FollowedStack.Screen name="ArtistDetails" component={ArtistStackNavigator}/>
        </FollowedStack.Navigator>
    )
}

const PlannedStack = createStackNavigator();

const PlannedStackNavigator = props => {
    return(
        <PlannedStack.Navigator>
            <PlannedStack.Screen name="Planned" component={Planned}/>
            <PlannedStack.Screen name="Show" options={{headerShown: false}} component={ShowStackNavigator}/>
        </PlannedStack.Navigator>
    )
}

const SearchStack = createStackNavigator();

const SearchStackNavigator = props => {
    return(
        <SearchStack.Navigator>
            <SearchStack.Screen name="Search" options={{headerShown: false}} component={Search}/>
            <SearchStack.Screen name="ArtistDetails" component={ArtistStackNavigator}/>
            <SearchStack.Screen name="Tour" component={TourStackNavigator}/>
        </SearchStack.Navigator>
    )
}

const HomeStack = createStackNavigator();

const HomeStackNavigator = props => {
    return (
        <HomeStack.Navigator>
            <HomeStack.Screen name={"Feed"} component={Home}/>
            <HomeStack.Screen name={"ArtistDetails"} component={ArtistStackNavigator}/>
            <HomeStack.Screen name={"Show"} component={ShowStackNavigator}/>
            <HomeStack.Screen name={"Followed"} component={FollowedStackNavigator}/>
            <HomeStack.Screen name={"Tour"} component={TourStackNavigator}/>
        </HomeStack.Navigator>
    )
};

const ShowStack = createStackNavigator();

const ShowStackNavigator = props => {
    let show = props.route.params.show,
        tour = findTourById(show.tour),
        artist = findArtistById(tour.artist);

    props.navigation.setOptions({
        headerShown: false
    });

    return(
        <ShowStack.Navigator>
            <ShowStack.Screen   options={{
                                    title: tour.name + " - " + show.location,
                                    headerTintColor: ComponentColor.TEXT_COLOR
                                }} 
                                name={"Show"} 
                                component={Show} 
                                initialParams={{show: show, tour: tour, artist: artist}}/>

            <ShowStack.Screen   options={{headerShown: false}}  name={"Tour"}
                                component={TourStackNavigator}/>
        </ShowStack.Navigator>
    )
};

const ArtistStack = createStackNavigator();

const ArtistStackNavigator = props => {
    let artist = props.route.params.artist;

    props.navigation.setOptions({
        headerShown: false
    });

    return(
        <ArtistStack.Navigator>
            <ArtistStack.Screen options={{
                                    title: artist.name,
                                    headerTintColor: ComponentColor.TEXT_COLOR
                                }}
                                name={"Artist"}
                                component={ArtistDetails}
                                initialParams={{artist: artist}}/>
            <ArtistStack.Screen options={{headerShown: false}}  name={"Tour"}
                                component={TourStackNavigator}/>

            <ArtistStack.Screen options={{headerShown: false}} name={"Show"}
                                component={ShowStackNavigator}/>
        </ArtistStack.Navigator>
    )
}

const TourStack = createStackNavigator();

const TourStackNavigator = props => {
    let tour =      props.route.params.tour,
        artist  =   findArtistById(tour.artist);

    props.navigation.setOptions({
        headerShown: false
    });

    return(
        <TourStack.Navigator>
            <TourStack.Screen   options={{
                                    title: tour.name,
                                    headerTintColor: ComponentColor.TEXT_COLOR,
                                }} 
                                name={"Tour"} 
                                component={Tour} 
                                initialParams={{tour: tour, artist: artist}}/>

            <TourStack.Screen   options={{headerShown: false}} name={"Show"}
                                component={ShowStackNavigator}/>
                                
            <TourStack.Screen   options={{headerShown: false}} name={"Artist"}
                                component={ArtistStackNavigator}/>
        </TourStack.Navigator>
    )
}


export default BottomNav;