import React, { useState, useEffect } from 'react';
import { TouchableOpacity, ImageBackground, View, Image, Text } from 'react-native';
import H2 from '../components/H2';
import styles from '../constants/Stylesheet';
import MainText from './MainText';
import SubText from './SubText';
import { findTourById, findArtistById } from '../func/Queries';
import H3 from './H3';
import { Tag } from './Tags';
import { getTextDate } from '../func/DateUtil';
import { ContentTitle, Border } from './Content';
import { TableColumnButton } from './Buttons';
import { FontAwesome } from '@expo/vector-icons';
import { ComponentColor } from '../constants/Colors';
import { LinearGradient } from 'expo-linear-gradient';

const Card = props => {

};

export const SmallCard = props => {
    return (
        <TouchableOpacity {...props} style={styles.smallCard} onPress={props.onPress}>
            <ImageBackground source={props.image} style={styles.smallCard_Image}></ImageBackground>
            <View style={styles.smallCard_Content}>
                {props.children}
            </View>
        </TouchableOpacity>
    )
};

export const LargeCard = props => {
    let tour    = props.tour;
    let artist  = props.artist;

    return (
        <TouchableOpacity {...props} style={styles.largeCard}>
            <ImageBackground source={{ uri: artist.artistImageCollection[0].url }} style={styles.largeCard_Image}>
                <LinearGradient style={styles.largeCard_Gradient} colors={["#000000", "transparent"]}>
                    <H2>{tour.name}</H2>
                </LinearGradient>

                <LinearGradient style={styles.largeCard_GradientBottom} colors={["transparent", "#000000"]}>
                    <MainText numberOfLines={2}>{tour.description}</MainText>
                </LinearGradient>
            </ImageBackground>
        </TouchableOpacity>
    )
}

export const ShowCard = props => {
    const show = props.show;
    const artist = props.artist;
    const tour = props.tour;

    return (
        <SmallCard onPress={props.onPress} image={{ uri: artist.artistImageCollection[0].url }}>
            <MainText>{tour.name}</MainText>
            <SubText>{show.location}</SubText>
        </SmallCard>
    )
}

export const DetailedShowCard = props => {
    const show = props.show;
    let date = getTextDate(show.date).split(" "),
        day = date[0],
        mon = date[1],
        yr = date[2];

    return (
        <TouchableOpacity {...props} style={[styles.detailedShowCard, { ...props.style }]}>
            <CalendarCard year={yr} month={mon} day={day}></CalendarCard>
            <View style={styles.detailedShowCard_info}>
                <H3>
                    {show.location}
                </H3>
                <MainText>
                    {show.venue}
                </MainText>
            </View>
        </TouchableOpacity>
    )
};

export const ArtistCard = props => {
    const artist = props.artist;

    let onTour;

    if (artist.ontour) {
        onTour = (
            <Tag importance={"ok"}>
                Touring
            </Tag>
        )
    }

    return (
        <TouchableOpacity {...props} style={[styles.artistCard, { ...props.style }]}>
            <Image style={styles.artistCard_image} source={{ uri: artist.artistImageCollection[0].url }} />
            <View style={styles.artistCard_name}>
                <H3>{artist.name}</H3>
                {onTour}
            </View>
        </TouchableOpacity>
    )
};

export const ArtistCardBig = props => {
    const artist = props.artist;

    return (
        <TouchableOpacity {...props} style={[styles.artistCardBig, { ...props.style }]}>
            <Image style={styles.artistCardBig_Image} source={{ uri: artist.images[0] }} />
            <MainText>{artist.name}</MainText>
        </TouchableOpacity>
    )
};

export const TourCardSmall = props => {
    const tour = props.tour;
    let artist = findArtistById(tour.artist);

    return (
        <TouchableOpacity {...props} style={[styles.artistCard, { ...props.style }]}>
            <Image style={[styles.artistCard_image, { borderRadius: 5 }]} source={{ uri: artist.images[0] }} />
            <View style={styles.artistCard_name}>
                <H3>{tour.name}</H3>
            </View>
        </TouchableOpacity>
    )
}

export const CalendarCard = props => {

    return (
        <View {...props} style={[styles.calendarCard, { ...props.style }]}>
            <View style={styles.calendarCard_top}>
                <Text style={styles.calendarCard_top_text}>
                    {props.year}
                </Text>
            </View>
            <View style={styles.calendarCard_bottom}>
                <Text style={styles.calendarCard_bottom_day}>
                    {props.day}
                </Text>
                <Text style={styles.calendarCard_bottom_month}>
                    {props.month}
                </Text>
            </View>
        </View>
    )
};

export const TourCard = props => {
    return (
        <View {...props} style={styles.TourCard}>
            <TableColumnButton onPress={props.onTourPress}>
                <H2>{props.tour.name}</H2>
            </TableColumnButton>
            <MainText numberOfLines={2} style={{ marginVertical: 5 }}>
                {props.tour.description}
            </MainText>
        </View>
    )
};

export const MoreCard = props => {

    return (
        <TouchableOpacity {...props} style={[styles.ArtistCardBig, styles.MoreCard]}>
            <FontAwesome name="plus" size={24} color={ComponentColor.TEXT_COLOR} />
        </TouchableOpacity>
    )
}
