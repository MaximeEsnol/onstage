import React from 'react';
import {Text} from 'react-native';
import styles from '../constants/Stylesheet';

const MainText = props => {
    return(
        <Text {...props} style={[styles.mainText, {...props.style}]}>
            {props.children}
        </Text>
    )
};

export default MainText;