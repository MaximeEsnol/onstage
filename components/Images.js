import React from 'react';
import { ImageBackground, ScrollView, View, Dimensions } from 'react-native';
import styles from '../constants/Stylesheet';

export const HeaderImage = props => {
    return (
        <ImageBackground style={[styles.HeaderImage, { ...props.style }]} source={props.image} resizeMode={"cover"}>
            {props.children}
        </ImageBackground>
    )
}

export const HeaderImages = props => {
    let images = props.images;

    let img_elems = [],
        view_width = (images.length * 100) + "%";

    for (let i = 0; i < images.length; i++) {
        img_elems.push(
            <HeaderImage style={{width: Dimensions.get("window").width}} image={{ uri: images[i].url }}></HeaderImage>
        );
    }
    
    return (
        <View style={{ width: "100%", aspectRatio: 16 / 9 }}>
            <ScrollView horizontal={true}
                contentContainerStyle={[{height: "100%", width: view_width, flexDirection: "row"}, { ...props.style }]}
                snapToInterval={Dimensions.get("window").width}
                decelerationRate={"fast"}>
                {img_elems}
            </ScrollView>
        </View>
    )
}