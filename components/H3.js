import React from 'react';
import {Text} from 'react-native';
import styles from '../constants/Stylesheet';

const H3 = props => {
    return(
        <Text style={styles.H3}>
            {props.children}
        </Text>
    )
}

export default H3;