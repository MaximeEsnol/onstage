import React from 'react';
import {View, Text} from 'react-native';
import styles from '../constants/Stylesheet';

export const Content = props => {
    return (
        <View style={[styles.content, {...props.style}]}>
            {props.children}
        </View>
    )
}

export const ContentTitle = props => {
    return(
        <Text style={[styles.content_title, {...props.style}]}>{props.children}</Text>
    )
}

export const ContentTable = props => {
    return(
        <View style={[styles.content_table, {...props.style}]} {...props}>
            {props.children}
        </View>
    )
}

export const ContentTableRow = props => {
    let len;
    if(props.children){
        len = props.children.length || 1;
    }
    let columnsWidth = 100 / len;
    let newChildren = [];

    for(let i = 0; i < len; i++){
        let col;

        if(len == 1){
            col = props.children;
        } else{
            col = props.children[i];
        }

        newChildren.push(
            React.cloneElement(col, {colWidth: columnsWidth + "%"})
        );
    }

    return (
        <View style={[styles.content_table_row, {...props.style}]} {...props}>
            {newChildren}
        </View>
    )
}

export const ContentTableColumn = props => {
    //if the colWidth prop does not exist, it means no ContentTableRow is present, since the value of 
    //the colWidth prop is calculated in that component.
    if(!props.colWidth){
        throw new Error("ContentTableColumn must be a child of ContentTableRow.");
    }
    return (
        <View style={[styles.content_table_col, {...props.style}, {width: props.colWidth}]} {...props}>
            {props.children}
        </View>
    )
}

export const Border = props => {
    return(
        <View style={styles.Border}>
            
        </View>
    )
}

