import React from 'react';
import {View, Text} from 'react-native';
import styles from '../constants/Stylesheet';

/**
 * Creates a tag which's color mainly depends on the importance set.
 * @param {*} props {{importance: String: The importance of this tag. It can be one of the following values. "ok", "mild", "risk", "warning"}}
 */
export const Tag = props => {
    let importance = props.importance;
    let fontColor, backgroundColor;

    switch(importance.toLowerCase()){
        case "ok":
            fontColor = "#ffffff";
            backgroundColor = "#5abf4a";
        break;
        case "mild":
            fontColor = "#000000";
            backgroundColor = "#fbfd87"
        break;
        case "risk":
            fontColor = "#ffffff";
            backgroundColor = "#f87d30";
        break;
        case "warning":
            fontColor = "#ffffff";
            backgroundColor = "#e94e4e";
        break;
        default: 
            fontColor = "#000000";
            backgroundColor = "#ffffff";
        break;
    }

    return(
        <View style={[{backgroundColor: backgroundColor}, styles.tag, {...props.style}]}>
            <Text style={[{color: fontColor}, styles.tagText, {...props.textStyle}]}>
                {props.children}
            </Text>
        </View>
    )
};