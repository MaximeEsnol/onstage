import React from 'react';
import {View, TextInput} from 'react-native';
import styles from '../constants/Stylesheet';

export const SearchField = props => {
    if(!props.onBlur){
        console.error("You must specify an onBlur property and callback when using SearchField.");
    }

    if(!props.onChangeText){
        console.error("You must specify an onChangeText property and callback when using SearchField.");
    }

    return (
        <TextInput {...props} style={[styles.SearchField, {...props.style}]}
                    autoCorrect={false}
                    autoFocus={true}
                    placeholder={"Search for an artist or tour..."}>

        </TextInput>
    )
};