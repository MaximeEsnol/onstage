import React from 'react';
import {TouchableOpacity, Text} from 'react-native';
import styles from '../constants/Stylesheet';

export const IconButton = props => {

    return(
        <TouchableOpacity style={[styles.IconButton, {...props.style}]} {...props}>
            {props.children}
        </TouchableOpacity> 
    )
};

export const TextButton = props => {

    return(
        <TouchableOpacity {...props} style={[styles.TextButton, {...props.style}]}>
            <Text style={[styles.TextButton_Text, {...props.textStyle}]}>
                {props.children}
            </Text>
        </TouchableOpacity>
    )
};

export const TableColumnButton = props => {
    return(
        <TouchableOpacity {...props} style={[styles.TableColumnButton, {...props.style}]}>
            {props.children}
        </TouchableOpacity>
    )
}

export const FollowButton = props => {
    let isFollowed = props.isFollowed;

    let text = (isFollowed) ? "Unfollow" : "Follow";

    const onPressAction = (isFollowed) ? () => props.unfollow() : () => props.follow();

    return(
        <TouchableOpacity {...props} onPress={onPressAction} style={[styles.FollowButton, {...props.style}]}>
            <Text style={styles.FollowButtonText}>
                {text}
            </Text>
        </TouchableOpacity>
    )
}