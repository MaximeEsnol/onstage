import 'react-native';
import React from 'react';
import {TextButton, FollowButton, TableColumnButton} from '../../components/Buttons.js';
import {Text} from 'react-native';

import renderer from 'react-test-renderer';

it('Renders a TextButton correctly', () => {
    const tree = renderer.create(
        <TextButton>I'm a text button</TextButton>
    ).toJSON();

    expect(tree).toMatchSnapshot();
});

it('Renders an Unfollow button correctly', () => {
    const tree = renderer.create(
        <FollowButton isFollowed={true}></FollowButton>
    ).toJSON();

    expect(tree).toMatchSnapshot();
});

it('Renders a Follow button correctly', () => {
    const tree = renderer.create(
        <FollowButton isFollowed={false}></FollowButton>
    ).toJSON();

    expect(tree).toMatchSnapshot();
});

it('Renders a TableColumnButton', () => {
    const tree = renderer.create(
        <TableColumnButton>
            <Text>Button</Text>
        </TableColumnButton>
    ).toJSON();

    expect(tree).toMatchSnapshot();
});