import * as Queries from "../../func/Queries.js";

const API = "http://94.225.245.39/OnStageWeb_war_exploded/api/";

beforeEach(() => {
    fetchMock.resetMocks();
});

test("findNearbyShows to call API", async () => {
    fetch.mockResponseOnce(JSON.stringify({id: 1}));

    const shows = await Queries.findNearbyShows("50.7774833", "4.2853212");

    expect(fetch).toHaveBeenCalledTimes(1);
    expect(fetch).toHaveBeenCalledWith(API + "shows/nearby?latitude=50.7774833&longitude=4.2853212");
});

test("findShowById to call API", async () => {
    fetch.mockResponseOnce(JSON.stringify({id:1}));

    const show = await Queries.findShowById(1);

    expect(fetch).toHaveBeenCalledTimes(1);
    expect(fetch).toHaveBeenCalledWith(API + "shows/id?id=1");
});

test("findTourById to call API", async () => {
    fetch.mockResponseOnce(JSON.stringify({id: 1}));

    const tour = await Queries.findTourById(1);

    expect(fetch).toHaveBeenCalledTimes(1);
    expect(fetch).toHaveBeenCalledWith(API + "tours/id?id=1");
});

test("findPickedTour to call API", async () => {
    fetch.mockResponseOnce(JSON.stringify({id: 1}));

    const tour = await Queries.findPickedTours();

    expect(fetch).toHaveBeenCalledTimes(1);
    expect(fetch).toHaveBeenCalledWith(API + "tours/toppick?topPicked=1");
});

test("findArtistById to call API", async () => {
    fetch.mockResponseOnce(JSON.stringify({id : 1}));

    const tour = await Queries.findArtistById(1);

    expect(fetch).toHaveBeenCalledTimes(1);
    expect(fetch).toHaveBeenCalledWith(API + "artists/id?id=1");
});

test("findAllShowsFromTour to call API", async () => {
    fetch.mockResponseOnce(JSON.stringify({id:1}));

    const shows = await Queries.findAllShowsFromTour(1);

    expect(fetch).toHaveBeenCalledTimes(1);
    expect(fetch).toHaveBeenCalledWith(API + "shows/tour?tourId=1");
});

test("findTourByArtist to call API", async () => {
    fetch.mockResponseOnce(JSON.stringify({id: 1}));

    const tours = await Queries.findToursByArtist(1);

    expect(fetch).toHaveBeenCalledTimes(1);
    expect(fetch).toHaveBeenCalledWith(API + "tours/artist?artistId=1");
});

test("findFirstNearShowFromTour to call API", async () => {
    fetch.mockResponseOnce(JSON.stringify({id: 1}));

    const show = await Queries.findFirstNearShowFromTour(1, "50.7774833", "4.2853212");

    expect(fetch).toHaveBeenCalledTimes(1);
    expect(fetch).toHaveBeenCalledWith(API + "shows/nearbyFromTour?latitude=50.7774833&longitude=4.2853212&tourId=1");
});

test("findArtistsNameLike to call API", async () => {
    fetch.mockResponseOnce(JSON.stringify({id:1}));

    const artists = await Queries.findArtistsNameLike("Dua");

    expect(fetch).toHaveBeenCalledTimes(1);
    expect(fetch).toHaveBeenCalledWith(API + "artists/search?search=Dua");
});

test("findToursNameLike to call API", async () => {
    fetch.mockResponseOnce(JSON.stringify({id: 1}));

    const tours = await Queries.findToursNameLike("Future");

    expect(fetch).toHaveBeenCalledTimes(1);
    expect(fetch).toHaveBeenCalledWith(API + "tours/search?name=Future");
});