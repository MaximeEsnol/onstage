import { getTextDate, getTimeFromDate } from "../../func/DateUtil";

test('Date "2020-06-11T23:00:00.000+00:00" to return 12 Jun 2020', () => {
    expect(getTextDate('2020-06-11T23:00:00.000+00:00')).toBe('12 Jun 2020');
});

test('Date "2019-12-31T23:00:00.000+00:00" to return 1 Jan 2020', () => {
    expect(getTextDate('2019-12-31T23:00:00.000+00:00')).toBe('01 Jan 2020');
});

test('Date "1970-01-01T17:30:00.000+00:00" to return 18:30', () => {
    expect(getTimeFromDate("1970-01-01T17:30:00.000+00:00")).toBe("18:30");
});