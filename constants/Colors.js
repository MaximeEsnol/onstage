/*
    Generic theme colors based on a color palette
*/
export const Color = {
    PRIMARY:                "#B33BF0",
    PRIMARY_LIGHT:          "#C263F3",
    PRIMARY_LIGHTER:        "#D591F7",
    PRIMARY_DARK:           "#A114E9",
    PRIMARY_DARKER:         "#9304DC",

    SECONDARY:              "#F6339E",
    SECONDARY_LIGHT:        "#F85DB2",
    SECONDARY_LIGHTER:      "#FA8EC9",
    SECONDARY_DARK:         "#F20888",
    SECONDARY_DARKER:       "#EB0080",

    TERNARY:                "#7247F0",
    TERNARY_LIGHT:          "#8E6CF4",
    TERNARY_LIGHTER:        "#B098F8",
    TERNARY_DARK:           "#5422EA",
    TERNARY_DARKER:         "#3F0ADE",
};

/*
    Specific colors assigned to different component variables. 
    This should be used in stylesheets before using 'Color.***'.
*/
export const ComponentColor = { 
    TEXT_COLOR:             "#FFFFFF",
    SUBTEXT_COLOR:          "#c3c0c8",
    HEADER_COLOR:           "#292829",
    BODY_BACKGROUND:        "#141314",
    CARD_BACKGROUND:        "#1f1f1f",
    BORDER_COLOR:           "#454145",
    BUTTON_COLOR:           Color.SECONDARY_DARK,
    FOLLOW_BUTTON_BORDER:   "#dbdbdb",
    FOLLOW_BUTTON_TEXT:     "#dbdbdb",
    FOOTER_COLOR:           "#292829",
    FOOTER_CONTENT_COLOR:   Color.SECONDARY,
    FOOTER_INACTIVE_COLOR:  "#b3b3b3",
    CALENDAR_BOTTOM:        "#ffffff",
    CALENDAR_TOP:           "#da3434",
    SEARCH_INPUT:           "#f7f7f7"
}

/**
 *  Theme for React Native Navigation.
 */
export const DefaultTheme = {
    dark: true,
    colors: {
        primary:        Color.PRIMARY,
        background:     ComponentColor.BODY_BACKGROUND,
        card:           ComponentColor.CARD_BACKGROUND,
        text:           ComponentColor.TEXT_COLOR,
        border:         ComponentColor.BORDER_COLOR
    }
};
