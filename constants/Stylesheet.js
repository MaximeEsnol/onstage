import React from 'react';
import {StyleSheet} from 'react-native';
import {Color, ComponentColor} from "./Colors";

const styles = StyleSheet.create({
    body: {
        backgroundColor: Color.BODY_BACKGROUND
    },
    cardContainer: {
        flexDirection: "row",
        flexWrap: "wrap",
        justifyContent: "space-around"
    },
    title: {
        fontSize: 26,
        fontWeight: "700",
        color: ComponentColor.TEXT_COLOR
    },
    H2: {
        fontSize: 20,
        fontWeight: "600",
        color: ComponentColor.TEXT_COLOR
    },
    H3: {
        fontSize: 18,
        fontWeight: "600",
        color: ComponentColor.TEXT_COLOR
    },
    mainText: {
        fontSize: 16,
        color: ComponentColor.TEXT_COLOR
    },
    SubText: {
        fontSize: 16,
        color: ComponentColor.SUBTEXT_COLOR
    },
    artistTitle_Follow: {
        flexDirection: "row",
        justifyContent: "space-between",
        alignItems: "center"
    },
    HeaderImage: {
        width: "100%",
        aspectRatio: 16/9
    },
    HeaderImages: {
        width: "100%",
        height: "100%"
    },
    IconButtons: {
        backgroundColor: "transparent",
        width: 48,
        aspectRatio: 1/1
    },
    FollowButton: {
        paddingVertical: 2,
        paddingHorizontal: 20,
        borderWidth: 1,
        borderColor: ComponentColor.FOLLOW_BUTTON_BORDER,
        borderRadius: 10,
        alignItems: "center",
        justifyContent: "center"
    },  
    FollowButtonText: {
        color: ComponentColor.FOLLOW_BUTTON_TEXT,
        fontSize: 16
    },
    TableColumnButton: {
        width: "100%",
        alignItems: "center"
    },
    Content_RightButton: {
        paddingVertical: 5,
        flexDirection: "row",
        alignItems: "center",
        borderBottomWidth: 1,
        borderBottomColor: ComponentColor.BORDER_COLOR,
    },
    Content_LeftSideLong: {
        width: "80%",
    },
    Content_RightSideShort: {
        width: "20%",
        alignItems: "center"
    },
    BuyTickets_Container: {
        alignItems: "center",
    },
    tag: {
        alignItems: "center",
        justifyContent: "center",
        borderRadius: 500,
        paddingHorizontal: 10,
        height: 20,
        marginTop: 4
    },  
    flexRow: {
        flexDirection: "row",
        alignItems: "center",
        justifyContent: "space-between"
    },
    SearchField: {
        backgroundColor: ComponentColor.SEARCH_INPUT,
        padding: 10,
        borderRadius: 5,
        elevation: 20
    },

    //content component styles start
    content: {
        marginHorizontal: 10,
        marginVertical: 5,
        paddingHorizontal: 5
    },
        content_title: {
            color: ComponentColor.TEXT_COLOR,
            fontSize: 22,
            fontWeight: "700",
            padding: 5
        },
        content_table: {
            width: "100%",
            flexDirection: "column",
            padding: 5
        }, 
            content_table_row: {
                flexDirection: "row",
                width: "100%",
                borderBottomWidth: 1,
                borderBottomColor: ComponentColor.BORDER_COLOR,
                justifyContent: "flex-start",
                alignItems: "flex-start"
            },
            content_table_col: {
                padding: 5,
                flexDirection: "column",
                alignItems: "center"
            },
    Border: {
        width: "100%",
        height: 1,
        backgroundColor: ComponentColor.BORDER_COLOR
    },
    //content component styles end

    //Cards start
    smallCard: {
        width: "45%",
        borderRadius: 5,
        backgroundColor: ComponentColor.CARD_BACKGROUND,
        overflow: "hidden",
        marginVertical: 5
    },
        smallCard_Image: {
            width: "100%",
            aspectRatio: 1/1
        },  
        smallCard_Content: {
            padding: 5
        },
    largeCard: {
        width: "100%",
        aspectRatio: 16/9,
        borderRadius: 5,
        elevation: 10,
        overflow: "hidden"
    },
        largeCard_Image: {
            width: "100%",
            height: "100%",
            resizeMode: "cover",
            backgroundColor: "white",
            marginVertical: 10,
        },
            largeCard_Gradient: {
                width: "100%",
                height: "25%",
                padding: 10,
                alignItems: "center"
            },
            largeCard_GradientBottom: {
                position: "absolute",
                width: "100%",
                bottom: 0,
                padding: 5,
                paddingBottom: 15
            },
    artistCard: {
        flexDirection: "row",
        width: "100%",
        paddingHorizontal: 5,
        paddingTop: 5,
        paddingBottom: 8,
        alignItems: "center",
        borderTopColor: ComponentColor.BORDER_COLOR,
        borderBottomColor: ComponentColor.BORDER_COLOR,
        borderTopWidth: 1,
        borderBottomWidth: 1,
        marginVertical: 5
    },
        artistCard_image: {
            width: 44,
            aspectRatio: 1/1,
            borderRadius: 500,
            marginRight: 10
        },
    artistCardBig: {
        width: 100,
        margin: 5,
        alignItems: "center"
    },
        artistCardBig_Image: {
            width: "100%",
            aspectRatio: 1/1,
            borderRadius: 500,
        },
    MoreCard: {
        width: 100,
        aspectRatio: 1/1,
        borderRadius: 500,
        margin: 5,
        justifyContent: "center",
        alignItems: "center",
        backgroundColor: ComponentColor.CARD_BACKGROUND
    },
    detailedShowCard: {
        flexDirection: "row",
        marginVertical: 5
    },
        detailedShowCard_info: {
            marginLeft: 10
        },
    calendarCard: {
        width: 45,
        height: 50,
        backgroundColor: ComponentColor.CALENDAR_BOTTOM,
        alignItems: "center",
        borderRadius: 2,
        overflow: "hidden"
    },
        calendarCard_top: {
            backgroundColor: ComponentColor.CALENDAR_TOP,
            width: "100%",
            alignItems: "center",
        },
            calendarCard_top_text: {
                color: ComponentColor.TEXT_COLOR,
                fontSize: 10
            },
        calendarCard_bottom: {
            width: "100%",
            alignItems: "center"
        },
            calendarCard_bottom_day: {
                fontSize: 16,
                fontWeight: "700"
            },
            calendarCard_bottom_month: {
                fontSize: 10
            },
    TourCard: {
        backgroundColor: ComponentColor.CARD_BACKGROUND,
        padding: 10,
        borderRadius: 5,
        elevation: 10,
        marginVertical: 10
    },
    //Cards end

    TextButton: {
        width: "100%",
        alignItems: "center",
        justifyContent: "center",
        backgroundColor: ComponentColor.BUTTON_COLOR,
        elevation: 5,
        padding: 10,
        marginVertical: 10,
        borderRadius: 500
    },
        TextButton_Text: {
            color: ComponentColor.TEXT_COLOR,
            fontWeight: "600",
            fontSize: 20
        },

});

export default styles;