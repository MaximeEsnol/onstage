import React, { useState } from 'react';
import { View, Text, ScrollView, StatusBar, SafeAreaView } from 'react-native';
import { SearchField } from '../components/FormComponents';
import { Content } from '../components/Content';
import { findToursNameLike, findArtistsNameLike } from '../func/Queries';
import { ArtistCard, TourCardSmall } from '../components/Cards';

const Search = props => {
    const [searchInput, setSearchInput] = useState("");
    const [searchInProgress, setSearchInProgress] = useState(false);
    const [results, setResults] = useState([]);

    const search = async() => {
        if(searchInput.length > 1 && !searchInProgress){
            console.log("start search");
            setSearchInProgress(true);
            let artists = await findArtistsNameLike(searchInput);
            let searchResults = [];
            console.log(artists);
            if(artists.error){
                console.log("error happened");
            } else {
                for(const artist of artists){
                    searchResults.push(
                        <ArtistCard artist={artist} key={artist.id + "-tour"}
                            onPress={() => props.navigation.navigate("ArtistDetails",
                                {
                                    artist: artist
                                })}></ArtistCard>
                    )
                }
            }
            
            setResults(searchResults);
            console.log("results set");
            setSearchInProgress(false);
        }
    }

    return (
        <ScrollView>
            <SafeAreaView>
                <StatusBar></StatusBar>
                <Content>
                    <SearchField onBlur={() => {
                        search();
                    }}
                        onChangeText={(text) => {
                            setSearchInput(text);
                        }}></SearchField>

                    {results}
                </Content>
            </SafeAreaView>
        </ScrollView>
    )
};

export default Search;