import React, { useEffect, useState } from 'react';
import { ScrollView, View } from 'react-native';
import { HeaderImage } from '../components/Images';
import { Content, ContentTitle } from '../components/Content';
import Title from '../components/Title';
import MainText from '../components/MainText';
import { findAllShowsFromTour, findArtistById } from '../func/Queries';
import styles from '../constants/Stylesheet';
import { ArtistCard, DetailedShowCard } from '../components/Cards';

const Tour = props => {

    const [showData, setShowData] = useState(null);
    const [showCards, setShowCards] = useState([]);
    const [artist, setArtist] = useState(null);
    const [cards, setCards] = useState(null);
    const [dataLoaded, setDataLoaded] = useState(false);

    let tour = props.route.params.tour;


    useEffect(() => {
        (async () => {
            if (!dataLoaded) {
                await loadTourScreen();
            }
        })();
    });

    const loadTourScreen = async () => {
        let artist = await findArtistById(tour.artist.id);
        let shows = await findAllShowsFromTour(tour.id);
        setArtist(artist);
        setShowData(shows);
        loadShowCards();
        setDataLoaded(true);
    };

    const loadShowCards = async () => {
        if (showCards.length == 0) {
            let arr = [];
            for (const show of showData) {
                arr.push(
                    <DetailedShowCard
                        key={"show-" + show.id}
                        show={show}
                        onPress={() => {
                            props.navigation.navigate("Show", {
                                show: show
                            });
                        }}>

                    </DetailedShowCard>
                )
            }
            setShowCards(arr);
        }
    }

    if (dataLoaded == true && artist != null && showData != null && cards == null) {
        let data = (
            <View>
                <HeaderImage image={{ uri: artist.artistImageCollection[0].url }}></HeaderImage>
                <Content>
                    <Title>{tour.name}</Title>
                    <MainText>
                        {tour.description}
                    </MainText>

                    <ArtistCard artist={artist}
                        onPress={() => {
                            props.navigation.navigate("Artist", {
                                artist: artist
                            });
                        }}></ArtistCard>

                    <View style={styles.flexRow}>
                        <ContentTitle>
                            Shows
                    </ContentTitle>
                        <ContentTitle>
                            {showData.length}
                        </ContentTitle>
                    </View>
                    {console.log(showCards)}
                </Content>
            </View>
        );

        setCards(data);
    }

    return (
        <ScrollView>
            {cards}
            <Content>
                {showCards}
            </Content>
        </ScrollView>
    )
};

export default Tour;