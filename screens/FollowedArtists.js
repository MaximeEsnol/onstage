import React, {useEffect, useState, useCallback} from 'react';
import {ScrollView, Text, View, RefreshControl} from 'react-native';
import { getPlannedShows, getDB, findShowById, findTourById, getFollowedArtists, findArtistById } from '../func/Queries';
import MainText from '../components/MainText';
import { Content, ContentTitle } from '../components/Content';
import {DetailedShowCard, ArtistCard} from '../components/Cards';
import { Color, ComponentColor } from '../constants/Colors';
import ArtistDetails from './ArtistDetails';

const FollowedArtists = props => {
    const db = getDB();
    const [refreshing, setRefreshing]                   = useState(false);
    const [followedArtistsJSON, setFollowedArtistsJSON] = useState(null);

    let followedArtists = [];

    const onRefresh = useCallback(() => {
        setRefreshing(true);

        setFollowedArtistsJSON(null);
        loadFollowedArtists().then(setRefreshing(false));
    });

    useEffect(() => {
        (async () => {
            if(!followedArtistsJSON){
                await loadFollowedArtists();
            }
        })();
    })

    const loadFollowedArtists = async () => {
        let data = await getFollowedArtists(db);
        console.log(data);
        setFollowedArtistsJSON(data);
    }

    if(followedArtistsJSON){
        let artists = require("../assets/artist.json");

        for(let i = 0; i < followedArtistsJSON.length; i++){
            let artist = findArtistById(followedArtistsJSON[i].artist_id, artists);
            console.log(artist);
            followedArtists.push(
                <ArtistCard artist={artist}
                               onPress={() => props.navigation.navigate("ArtistDetails", {
                                   artist: artist
                               })}></ArtistCard>
            );
        }
    }

    return(
        <ScrollView refreshControl={<RefreshControl 
            title={"Looking for new shows"} 
            titleColor={ComponentColor.TEXT_COLOR} 
            tintColor={Color.SECONDARY}  
            colors={[Color.SECONDARY, Color.PRIMARY]} 
            refreshing={refreshing} 
            onRefresh={() => onRefresh()}/>}>
            <Content>
                {followedArtists}
            </Content>
        </ScrollView>
    )
};

export default FollowedArtists;