import React, {useEffect, useState} from 'react';
import * as SQLite from 'expo-sqlite';
import MainStackNavigator from './navigation/AppNavigation';
import {initDB, getDB} from './func/Queries';

export default function App() {
    //Initialize database when the app loads.

    const [dbSetup, setDbSetup] = useState(false);
    const db = getDB();

    useEffect(() => {
        (async () => {
            if(!dbSetup){
                await initDB(db);
                setDbSetup(true);
            }
        })();
    })

   
    
    /*
     * Coords in shows.json are displayed in the following format: 
     * latitude,longitude 
     */
  return (
      <MainStackNavigator/>
  );
}
