import * as SQLite from 'expo-sqlite';

const API = "http://94.225.245.39/OnStageWeb_war_exploded/api/";



export const findNearbyShows = async (userLatitude, userLongitude) => {
    let request = await fetch(API + "shows/nearby?latitude=" + userLatitude + "&longitude=" + userLongitude);
    let nearbyShows = await request.json();
    return nearbyShows;
}

export const getTourAndArtistFromShow = async (show) => {
    let arr;

    let tour = await findTourById(show.id);

    let artist = await findArtistById(tour.id);

    arr["tour"] = tour;
    arr["artist"] = artist;
    arr["show"] = show;

    return arr;
}

export const findShowById = async (id) => {
    let request = await fetch(API + "shows/id?id=" + id);
    return await request.json();
}

/**
 * Finds a tour from the tours.json file by its ID. 
 * 
 * @param {Number} tourId The ID of the desired tour.
 * 
 * @return {Object} This function returns the tour object in JSON format if a tour 
 * with the given ID was found, or false if no tour was found.
 */
export const findTourById = async (tourId) => {
    let request = await fetch(API + "tours/id?id=" + tourId);
    return await request.json();
};

export const findPickedTours = async  () => {
    let request = await fetch(API + "tours/toppick?topPicked=1");
    return await request.json();
}

/**
 * Finds an artist from the artist.json file by their ID.
 * 
 * @param {Number} artistId The ID of the desired artist.
 * 
 * @return {Object} This function returns the artist object in JSON format if an artist 
 * with the given ID was found, or false if no artist was found.
 */
export const findArtistById = async (artistId) => {
    let request = await fetch(API + "artists/id?id=" + artistId);
    return await request.json();
}

/**
 * Finds all the shows from a given tour.
 * 
 * @param {Number} tourId The ID of the tour of which you want to 
 * find the shows from.
 * 
 * @return {Array} An array consisting of all the shows on this tour. If this tour has no shows or no 
 * shows were found, an empty array is returned.
 */
export const findAllShowsFromTour = async (tourId) => {
    let request = await fetch(API + "shows/tour?tourId=" + tourId);
    let response = await request.json();
    return response;
}

/**
 * Finds all the tours from a specific artist.
 * 
 * @param {Number} artistId The ID of the artist of which you want to retrieve 
 * the tours.
 * 
 * @return {Array} An array with all the tours from this artist. If this artist 
 * has no tours, or no tours were found, an empty array is returned.
 */
export const findToursByArtist = async artistId => {
    let request = await fetch(API + "tours/artist?artistId=" + artistId);
    return await request.json();
}

/**
 * Finds the first show that is located inside of the given radius. 
 * Note: the first show does not mean the first show in time, but rather 
 * the first show this function comes accross when looping through 
 * all the shows of the given tour. 
 * 
 * @param {Number} tourId The ID of the tour of which you want to retieve
 * the first near show of.
 * @param {double} userLat The latitude from the user's (approximate) location.
 * @param {double} userLong The longitude from the user's (approximate) location.
 */
export const findFirstNearShowFromTour = async (tourId, userLat, userLong) => {
    let request = await fetch(API + "shows/nearbyFromTour?latitude=" + userLat + "&longitude=" + userLong + "&tourId=" + tourId);
    return await request.json();
};

export const findArtistsNameLike = async name => {
    let request = await fetch(API + "artists/search?search=" + name);
    return await request.json();
};

export const findToursNameLike = async name => {
    let request = await fetch(API + "tours/search?name=" + name);
    return await request.json();
}

export const getDB = () => {
    return SQLite.openDatabase("onstage-db");
}

export const initDB = async (db) => {
    let result = await runQuery(db,
        "create table if not exists planned (id integer primary key autoincrement not null, show_id integer not null); "
    );

    result = await runQuery(db,
        "create table if not exists followed (id integer primary key autoincrement not null, artist_id integer not null); "
    );
};

export const addShowToPlanned = async (db, id) => {
    let result = await runQuery(
        db,
        "INSERT INTO planned (show_id) VALUES (?)",
        [id]
    );
};

export const removeShowFromPlanned = async (db, id) => {
    let result = await runQuery(
        db,
        "DELETE FROM planned WHERE show_id = ?",
        [id]
    );
};

export const isShowPlanned = async (db, id) => {
    let rows = await runQuery(
        db,
        "SELECT show_id FROM planned WHERE show_id = ?",
        [id]
    );
    //if the length of the returned rows is larger than 0, it means the show is already planned.
    return (rows.length > 0);
}

export const getPlannedShows = async (db) => {
    let rows = await runQuery(
        db,
        "SELECT * FROM planned ORDER BY id DESC"
    );
    return rows;
};

export const getFollowedState = async (db, artistId) => {
    let rows = await runQuery(
        db,
        "SELECT * FROM followed WHERE artist_id = ?",
        [artistId]
    );

    //if the length of the returned rows is larger than 0, it means the artist is already followed
    return (rows.length > 0);
}

export const addFollow = async (db, artistId) => {
    let rows = await runQuery(
        db,
        "INSERT INTO followed (artist_id) VALUES (?)",
        [artistId]
    );
}

export const removeFromFollow = async (db, artistId) => {
    let rows = await runQuery(
        db,
        "DELETE FROM followed WHERE artist_id = ?",
        [artistId]
    );
}

export const getFollowedArtists = async (db, limit = 5) => {
    let rows = await runQuery(
        db,
        "SELECT * FROM followed LIMIT ?",
        [limit]
    );

    return rows;
}

/**
 * Executes a query and returns the rows returned by the query after the promise 
 * was resolved. If an error occured the promise is rejected and the error message is
 * returned. 
 * 
 * @param {*} db The SQLite database to be used for this query.
 * @param {String} query The query to be executed, values can be replaced with '?' symbols which will 
 * then be replaced by SQLite with the values provided in the params array.
 * @param {Array} params Array of parameters containing the values for the '?'.
 * 
 * @return {Promise} A promise that resolves when no error occurs during the execution of the query. 
 * If the promise resolves, the rows that the database returned are returned. If an error occurred, 
 * the promise is rejected and the error message is returned instead.
 */
const runQuery = async (db, query, params = []) => {
    return new Promise((resolve, reject) => {
        db.transaction(
            transaction => {
                transaction.executeSql(
                    query,
                    params,
                    (_, { rows: { _array } }) => {
                        resolve(_array);
                    }
                )
            },
            error => {
                reject(error);
            }
        );
    });
}

/**
 * Calculates whether the second point, identified by a latitude and longitude value, is 
 * within the given radius of the first point, also identified by a latitude and longitude value.
 * 
 * @param {Number} radius The radius, with the first point as center, in which the second point should be in.
 * @param {Number} latitude1 The latitude of the first point.
 * @param {Number} longitude1 The longitude of the first point.
 * @param {Number} latitude2  The latitude of the second point.
 * @param {Number} longitude2 The longitude of the second point.
 * 
 * @return {boolean} true if the second point is within the given radius or false if it is not.
 */
const isInRadius = (radius, latitude1, longitude1, latitude2, longitude2) => {
    // √( (latitude - userLatitude)^2 + (longitude - userLongitude)^2)
    // Take the absolute value of the result
    let differenceLat = latitude1 - latitude2,
        differenceLong = longitude1 - longitude2;

    let powLat = differenceLat * differenceLat,
        powLong = differenceLong * differenceLong;

    let distance = Math.sqrt(powLat + powLong);

    if (Math.abs(distance) <= radius) return true;

    return false;
}