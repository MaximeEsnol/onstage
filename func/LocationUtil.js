import * as Permissions from 'expo-permissions';
import * as Location from 'expo-location';
const NetworkInfo = require('react-native-network-info');

const IP_API_KEY = "9eccbc7c18e34f699547aa496ab9bb9d";

async function askLocationPermission(){
    const {status, expires, permission} = await Permissions.askAsync(Permissions.LOCATION);

    if(status === "granted"){
        return true;
    } else {
        return false;
    }
}

export default async function getLocation(){
    let canAccessLocation = await askLocationPermission();
    let location;
    if(canAccessLocation){
        location = await Location.getCurrentPositionAsync();
    } else {
        NetworkInfo.getIPAddress(ip => {
            console.log(ip);
          });
        // location = {coords: {latitude: "", longitude: ""}};
        // let ip_location = await fetch("https://api.ipgeolocation.io/ipgeo?apiKey="+IP_API_KEY+"&ip=" + ip);
        // location.coords.latitude = ip_location.latitude;
        // location.coords.longitude = ip_location.longitude;
    }

    return location;
}
