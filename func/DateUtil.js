/**
 * Generates a readable string based off the provided date. 
 * The generated string looks like '11 Feb 1999'.
 * 
 * @param {string} dbDateFormat A string representing a date in the following format: "2020-11-02T23:00:00.000+00:00", other formats will
 * not work.
 * @return {string} A more readable version of the provided date. For example '11 Feb 1999'.
 */
export const getTextDate = (dbDateFormat) => {
    let parsedDate = dbDateFormat.substr(0, 10);
    let dateParts = parsedDate.split("-"),
        year =  dateParts[0],
        month = dateParts[1],
        day =   dateParts[2],
        date =  new Date();

    date.setFullYear(year, month-1, day);
    date.setUTCHours(23, 0, 0);
    let textDateParts = date.toString().split(" ");

    return "" + textDateParts[2]  //the day number
        + " " + textDateParts[1]  //the month abbreviation
        + " " + textDateParts[3]; // the year number
}

/**
 * Parses the time out of a date string. The date string should look 
 * like this: 1970-01-01T18:30:00.000+00:00.
 * The function takes the first 5 characters coming after the 'T' character.
 * If no 'T' character is found in dateString, then the dateString 
 * is returned without changes.
 * 
 * This function does NOT consider timezones, it just takes the plain value of time 
 * out of the date string.
 * 
 * @param {string} dateString The string of the date to parse the time out.
 */
export const getRawTimeFromDateString = (dateString) => {
    let pos = dateString.indexOf("T");
    if(pos != -1){
        return dateString.substr(pos+1, 5);
    }
    return dateString;
}

/**
 * Gets the time from a date string, by creating a JS Date object from 
 * the given date string, and then getting the hours and the minutes of the date. 
 * 
 * This function does consider the timezones.
 * 
 * @param {string} dateString The string of the date to get the time from.
 */
export const getTimeFromDate = (dateString) => {
    let date = new Date(dateString);

    return "" + date.getHours() + ":" + date.getMinutes();
}